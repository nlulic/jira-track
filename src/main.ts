import yargs = require("yargs");

import {
  startTracking,
  stopTracking,
  configure,
  replace, 
  publish,
  status,
  prune, 
  edit
} from "./commands";

const args = yargs
  .recommendCommands()
  .command(configure)
  .command(startTracking)
  .command(stopTracking)
  .command(edit)
  .command(replace)
  .command(status)
  .command(publish)
  .command(prune)
  .exitProcess(true)
  .demandCommand()
  .showHelpOnFail(true)
  .strict()
  .help();

async function main(argv: string[]) {

  try {

    return args.parse((argv || process.argv).slice(2));

  } catch (error) {
    console.error(error);
  }
}

export default main; 
