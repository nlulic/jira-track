import { isDate } from "../common/validators";
import { CommandModule, Argv } from "yargs";
import { storageClient } from "../core";
import { whereDate } from "./status";
import { format } from "date-fns";
import { toUpper } from "lodash";

const toUpperCase = (str: string): string => toUpper(str);

export const replace: CommandModule = {
  describe: "Replaces all occurrences of an issue key for a given date",
  command: "replace <issue-key> <new-issue-key>",
  builder: (argv: Argv) => argv
    .positional("issue-key", { type: "string" }).coerce("issue-key", toUpperCase)
    .positional("new-issue-key", { type: "string" }).coerce("new-issue-key", toUpperCase)
    .option("date", { alias: "d", default: new Date() }).check(isDate()),
  async handler({ issueKey, newIssueKey, date: dateInput }) {

    const date = new Date(dateInput as string);

    const relevantWorklogs = storageClient
      .getWorklogs()
      .filter(whereDate(date))
      .filter(worklog => issueKey === toUpper(worklog.issueKey))

    if (!relevantWorklogs.length)
      return console.log(`No worklogs with the issue key ${issueKey} found on ${format(date, "dd.MM.yyyy")}`)

    for (const worklog of relevantWorklogs) {
      storageClient.updateWorklog({ ...worklog, issueKey: newIssueKey as string });
    }
  }
}