import yargs, { CommandModule, Argv } from "yargs";
import { storageClient } from "../core";
import { range } from "lodash";

export const configure: CommandModule = {
  describe: "Allows you to modify the internal configuration of jira-track",
  command: "configure",
  aliases: ["c", "conf"],
  builder(argv: Argv) {
    return argv.option("round", { alias: "r", choices: range(1, 31), type: "number" })
      .option("jira-url", { alias: "url", type: "string" })
      .option("username", { alias: "u", type: "string" });
  },
  async handler({ username, jiraUrl, round }) {

    const configuration = storageClient.getConfiguration();

    storageClient.updateConfiguration({
      ...configuration,
      roundToNearestMinutes: round as number || configuration.roundToNearestMinutes,
      username: username as string || configuration.username,
      jiraUrl: jiraUrl as string || configuration.jiraUrl
    });
  
    process.stdout.write(`Configuration changes have been applied\n`); 
  }
}