import { isNumber, isValidTimeFormat, isValidTimeFormatOrEmpty } from "../common/validators";
import { askWithPlaceholder, danger, bold } from "../common/console";
import { dateToTimeString, timeStringtoDate } from "../common/time";
import { storageClient, Worklog } from "../core";
import { CommandModule, Argv } from "yargs";
import { createInterface } from "readline";

export const edit: CommandModule = {
  describe: "allows you to modify the logged time. Get the worklog id by running \`jira-track status --with-ids\`",
  command: "edit <worklog-id>",
  aliases: ["e"],
  builder: (argv: Argv) => argv
    .positional("worklog-id", { type: "number" }).check(isNumber("worklog-id"))
    .option("start", { alias: "s", type: "string" }).check(isValidTimeFormatOrEmpty("start"))
    .option("end", { alias: "e", type: "string" }).check(isValidTimeFormatOrEmpty("end"))
    .option("comment", { alias: "c", type: "string" })
    .option("key", { alias: "k", type: "string" }),
  async handler({ worklogId, start, end, comment, key }) {

    let worklog = storageClient.getWorklog(worklogId as number);

    if (!worklog)
      return process.stderr.write(danger(`Worklog with the id ${worklogId} does not exist. \n`));

    const rl = createInterface(process.stdin, process.stdout, undefined, true);

    const { startAt, endAt, issueKey, comment: worklogComment } = worklog;

    // curry with rl param
    const ask = askWithPlaceholder.bind(undefined, rl);

    const fieldValues: Partial<Record<keyof Worklog, string>> = {
      issueKey: key as string || await ask(bold("issue:"), issueKey),
      comment: comment as string || await ask(bold("comment:"), worklogComment || ""),
      startAt: start as string || await ask(bold("start:"), dateToTimeString(startAt), { "dateFormatInvalid": isValidTimeFormat }),
      endAt: end as string || await ask(bold("end:"), dateToTimeString(endAt || new Date()), { "dateFormatInvalid": isValidTimeFormat })
    }

    rl.close();

    const parsers: Partial<Record<keyof Worklog, (str: string) => unknown>> = {
      startAt: (time: string) => timeStringtoDate(time, startAt),
      endAt: (time: string) => timeStringtoDate(time, endAt),
    }

    for (const field in fieldValues) {

      const worklogField = field as keyof Worklog,
        parser = parsers[worklogField];

      if (!parser) {
        worklog[worklogField] = fieldValues[worklogField] as never;
        continue;
      }

      worklog[worklogField] = parser(fieldValues[worklogField] as string) as never;
    }

    storageClient.updateWorklog(worklog);
  }
}