import { createDataTable, roundToNearest, formatDelta, isDate, success, highlight } from "../common";
import { storageClient, Worklog } from "../core";
import { isSameDay, format } from "date-fns";
import { CommandModule, Argv } from "yargs";
import { table } from "table";
import { sum } from "lodash";

export const status: CommandModule = {
  describe: "Displays a report of the worked time for a given date (default: Today)",
  command: "status [--date]",
  aliases: ["report"],
  builder(argv: Argv) {
    return argv
      .option("date", { alias: "d", default: new Date() }).check(isDate())
      .option("with-ids", { default: false, type: "boolean" })
      .option("raw", { default: false, type: "boolean" })
  },
  async handler({ date: dateInput, raw, withIds }) {

    const date = new Date(dateInput as string);

    const relevantWorklogs = storageClient
      .getWorklogs()
      .filter(whereDate(date));

    const { roundToNearestMinutes } = storageClient.getConfiguration();

    const round = raw ? 0 : roundToNearestMinutes;

    const dataTable = createDataTable(
      relevantWorklogs,
      [
        withIds && ["ID", ({ id }) => id],
        ["Period", ({ startAt, endAt }) => `${format(roundToNearest(startAt, round), "HH:mm")} - ${endAt ? format(roundToNearest(endAt, round), "HH:mm") : "NOW"}`],
        ["Worked", ({ startAt, endAt = new Date() }) => {
          const delta = roundToNearest(endAt, round).getTime() - roundToNearest(startAt, round).getTime();
          return formatDelta(delta);
        }],
        ["Issue", ({ issueKey }) => issueKey],
        ["Comment", ({ comment }) => comment && comment.slice(0, 40).trim() + "..."],
        ["Published", ({ published }) => published ? "YES" : "NO"]
      ]
    );

    process.stdout.write(highlight(`STATUS FOR ${format(date, "dd.MM.yyyy")}\n`));

    process.stdout.write(
      table([
        dataTable.columns,
        ...dataTable.rows.map(row => [
          row[0],
          row[1],
          row[2],
          row[3],
          row[4],
          withIds && row[5]
        ].filter(withoutFalse())), [
          withIds && "",
          "Total:", success(formatDelta(
            sum(relevantWorklogs.map(({ startAt, endAt = new Date() }) => roundToNearest(endAt, round).getTime() - roundToNearest(startAt, round).getTime()))
          )),
          "",
          "",
          ""
        ].filter(withoutFalse())
      ], {
        columns: {
          0: { alignment: 'left', },
          1: { alignment: 'left', },
          2: { alignment: 'left', },
          4: { alignment: 'left', }
        }
      })
    );

    process.stdout.write(`Run ${highlight("jira-track publish")} to publish unpublished worklogs to jira\n`);
  }
}

export function whereDate(date: Date) {
  return ({ startAt }: Worklog) => {
    return isSameDay(new Date(startAt), date);
  }
}

function withoutFalse() {
  return (value: any): boolean => {
    return value !== false;
  }
}