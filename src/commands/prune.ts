import { CommandModule, Argv } from "yargs";
import { createInterface } from "readline";
import { ask, highlight } from "../common";
import { storageClient } from "../core";
import { toUpper } from "lodash";

export const prune: CommandModule = {
  describe: "Deletes all published worklogs from the database",
  command: "prune",
  aliases: ["clear"],
  builder(argv: Argv) {
    return argv.options("all", { default: false, type: "boolean" })
  },
  async handler({ all }) {

    if (all) {
      const rl = createInterface(process.stdin, process.stdout, undefined, true);

      const canContinue = await ask(rl, `By using ${highlight("--all")} even the unpublished worklogs will be removed. Are you sure you want to continue? (y/N)\n`);

      rl.close();

      if (toUpper(canContinue) !== "Y") 
        return;
    }

    const worklogs = all ? [] : storageClient.getWorklogs().filter(({ published }) => !published);
    storageClient.updateWorklogs(worklogs);

    process.stdout.write(`Worklogs have been cleared: ${worklogs.length} worklogs left.\n`)
  }
}