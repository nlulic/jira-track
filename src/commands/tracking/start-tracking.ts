import { success, bold } from "../../common/console/text"
import { Worklog, storageClient } from "../../core";
import { ask, getTime } from "../../common";
import { CommandModule, Argv } from "yargs";
import { createInterface } from "readline";
import { toUpper } from "lodash";

export const startTracking: CommandModule = {
  describe: "Starts the time tracking for a given issue key",
  command: "start <jira-issue-key>",
  aliases: ["s", "track"],
  builder: (argv: Argv) => argv.positional("jira-issue-key", { demandOption: true }),
  async handler({ jiraIssueKey }) {

    const nextWorklog: Worklog = {
      issueKey: toUpper(jiraIssueKey + ""),
      startAt: new Date(),
      id: getTime()
    };

    const allWorklogs = storageClient.getWorklogs();

    // if there are no worklogs, or the last worklog already has an end time, add the worklog
    if (!allWorklogs.length || allWorklogs[allWorklogs.length - 1].endAt) {
      storageClient.updateWorklogs(allWorklogs.concat(nextWorklog))
      console.log(success(`Started tacking work on ${toUpper(jiraIssueKey + "")} ⏳`));
      return;
    }

    const prevWorklog = allWorklogs.pop() as Worklog;

    // add a comment for the previous worklog 
    const rl = createInterface(process.stdin, process.stdout, undefined, true),
      comment = await ask(rl, `Write a comment for your work on your ${bold("previous")} ticket (${prevWorklog.issueKey})\n`);

    storageClient.updateWorklogs(
      allWorklogs.concat({ ...prevWorklog, comment, endAt: new Date() }, nextWorklog)
    );

    console.log(bold(`Stopped tracking ${prevWorklog.issueKey} ⌛`));
    console.log(success(`Started tacking work on ${toUpper(jiraIssueKey + "")} ⏳`));

    rl.close();
  }
}