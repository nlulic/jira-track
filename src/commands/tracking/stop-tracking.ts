import { Worklog, storageClient } from "../../core";
import { bold } from "../../common/console/text"; 
import { createInterface } from "readline";
import { CommandModule } from "yargs";
import { ask } from "../../common";

export const stopTracking: CommandModule = {
  describe: "Stops the time tracking for the latest active issue",
  command: "stop",
  aliases: ["end"],
  async handler() {

    const worklogs = storageClient.getWorklogs();

    if (!worklogs.length || worklogs[worklogs.length - 1].endAt) {
      process.stdout.write(bold("There are currenlty no open worklogs"));
      return
    }

    const lastWorklog = worklogs.pop() as Worklog;

    // add a comment for the previous worklog 
    const rl = createInterface(process.stdin, process.stdout, undefined, true),
      comment = await ask(rl, `Write a comment for your work on ticket ${bold(lastWorklog?.issueKey)}\n`);

    storageClient.updateWorklogs(
      worklogs.concat([{ ...lastWorklog, comment, endAt: new Date() }])
    );

    console.log(bold(`Stopped tracking ${lastWorklog.issueKey} ⌛`));

    rl.close();
  }
}