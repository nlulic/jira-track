import { isDate, askForCredentials, roundToNearest, ask } from "../common";
import { storageClient, publishWorklogs, JiraContext } from "../core";
import { highlight, success, danger } from "../common/console/text";
import { cursorTo, createInterface } from "readline";
import { CommandModule, Argv } from "yargs";
import { whereDate } from "./status";
import { toUpper } from "lodash";

export const publish: CommandModule = {
  describe: "Publishes tracked worklogs to JIRA",
  command: "publish [--date]",
  aliases: "p",
  builder(argv: Argv) {
    return argv.option("username", { alias: "u" })
      .option("jira-url", { alias: "url" })
      .option("date", { alias: "d", default: new Date() })
      .options("raw", { default: false, type: "boolean" })
      .check(isDate())
  },
  async handler({ username, jiraUrl, date, raw }) {

    if (raw) {
      const rl = createInterface(process.stdin, process.stdout, undefined, true);

      const canContinue = await ask(rl, `By using ${highlight("--raw")} the tracked hours will not be rounded. Are you sure you want to continue? (y/N)\n`);

      rl.close();

      if (toUpper(canContinue) !== "Y")
        return;
    }

    const { username: configuredUsername, jiraUrl: configuredUrl } = storageClient.getConfiguration(),
      credentials = await askForCredentials({ username: username as string || configuredUsername }),
      url = jiraUrl as string || configuredUrl;

    if (!url)
      throw new Error("Please add your JIRA url to the configuration or pass it with the `jira-url` option");

    const { roundToNearestMinutes: nearest } = storageClient.getConfiguration();


    const worklogs = storageClient.getWorklogs()
      .filter(whereDate(new Date(date as string)))
      // don't publish already published worklogs
      .filter(({ published }) => !published)
      // only worklogs that are not being tracked
      .filter(({ endAt }) => !!endAt)
      .map(worklog => raw ? worklog : ({
        ...worklog,
        startAt: roundToNearest(worklog.startAt, nearest),
        endAt: roundToNearest(<Date>worklog.endAt, nearest)
      }));

    const context: JiraContext = { credentials, jiraUrl: url }

    if (!worklogs.length) {
      process.stdout.write(success("Nothing to publish\n"))
      return;
    }

    try {

      let published: number[] = [];

      for await (const publishingResult of publishWorklogs(worklogs, context)) {

        const { virtualWorklogId } = publishingResult;

        published.push(virtualWorklogId);
        storageClient.updatePublishedWorklog(virtualWorklogId);

        cursorTo(process.stdout, 0);
        process.stdout.write(`Publishing... ${(published.length / worklogs.length * 100).toFixed(2)}% ${published.length % 2 ? "⌛" : "⏳"}`);
      }


    } catch (error) {
      process.stdout.write(danger(`Publishing to ${url} failed! ${error.message || JSON.stringify(error)}`));
    }

    process.stdout.write(success("\nFinished publishing worklogs\n"))
  }
}