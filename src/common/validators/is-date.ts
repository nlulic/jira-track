import { isValid } from "date-fns"; 

export function isDate() {
  return ({ date }: { date: string | Date }) => {
    if (isValid(new Date(date)))
      return true;

    throw new Error(`Invalid date ${date} given. Please use a valid date format`);
  }
};