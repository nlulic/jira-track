import { Arguments } from "yargs";

export function isValidTimeFormatOrEmpty(argName: string) {
  return (args: Arguments) => {

    const value = args[argName] as string;

    // allow undefined and null
    if(value === undefined || value === null)
      return true; 

    if (isValidTimeFormat(value))
      return true;

    throw new Error(`Invalid time format ${value} passed for option \`${argName}\`. Please use the format hh:mm to edit times`);
  }
}

/**
 * @param time expects a time following the `hh:mm` i.e 15:15, 05:15, 5:15
 */
export function isValidTimeFormat(time: string): boolean {

  const TIME_REGEX = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/;

  return TIME_REGEX.test(time);
}