import { Arguments } from "yargs";
import { isNumber as isNum, isNaN } from "lodash";

export function isNumber(argName: string) {
  return (args: Arguments) => {

    const value = args[argName];

    if(isNum(value) && !isNaN(value))
      return true; 

    
    console.log("what!!!", value, argName); 

    throw new Error(`Invalid number ${value} given. Please use a valid number`); 
  }
}