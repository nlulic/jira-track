import { startOfDay } from "date-fns";
import { timeToMs } from "./time-to-ms";

/**
 * @param time `hh:mm` formatted string represeting the time
 * @param date date to which the time will be appended.
 */
export function timeStringtoDate(time: string, date: string | Date = new Date()): Date {

  if (!(date instanceof Date))
    date = new Date(date);

  const dateMs = startOfDay(date).getTime(),
    timeMs = timeToMs(time);

  return new Date(dateMs + timeMs);
}