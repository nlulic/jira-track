/**
 * @param time expects a time following the `hh:mm` i.e 15:15, 05:15, 5:15
 */
export function timeToMs(time: string): number {

  const [hours, minutes] = time.split(":").map(Number);

  return (hours * (60000 * 60)) + (minutes * 60000);
}