import { roundToNearestMinutes } from "date-fns";

export function roundToNearest(date: Date | string, nearestTo: number) {

  if (nearestTo < 2)
    return new Date(date);

  return roundToNearestMinutes(new Date(date), { nearestTo });
}