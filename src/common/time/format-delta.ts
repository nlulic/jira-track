import { format, addHours } from "date-fns";

const ONE_HOUR = 60;

export function formatDelta(deltaMs: number) {

  const minutes = deltaMs / 1000 / 60,
    formatString = minutes < ONE_HOUR ? "m'm'" : "H'h' m'm'";

  return format(
    addHours(new Date(deltaMs), -1), 
    formatString
  ); 
}