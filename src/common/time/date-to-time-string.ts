import { format } from "date-fns"; 

export function dateToTimeString(date: string | Date): string {

  if (!(date instanceof Date))
    date = new Date(date);

  return format(date, "HH:mm");
}