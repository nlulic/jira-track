export function createDataTable<T, TKey extends string>(data: T[], definitions: Array<[TKey, (entry: T) => any | undefined]>) {


  const rows = data.map((d) => definitions.filter(Boolean).map((definition: any) => definition[1](d)));
  return {
    columns: definitions.filter(Boolean).map((definition) => definition[0]),
    rows
  };
}