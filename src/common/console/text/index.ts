import chalk from "chalk";

const bold = (str: string): string => chalk.bold(str); 

const danger = (str: string): string => chalk.red(str); 

const success = (str: string): string => chalk.green(str); 

const highlight = (str: string): string => chalk.bgGrey(str); 

export {
  bold,
  danger, 
  success, 
  highlight
}