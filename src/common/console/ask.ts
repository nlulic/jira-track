import { Interface } from "readline";

export async function ask(
    rl: Interface,
    question: string,
    validators: { [validator: string]: (input: string) => boolean } = {}
): Promise<string> {

    const input = await new Promise<string>((resolve) => rl.question(`${question} `, resolve));

    for (const validator in validators) {

        const isValid = validators[validator].bind(undefined, input);

        if (isValid())
            continue;

        throw new Error(`${validator}: ${input}`);
    }

    return input;
}