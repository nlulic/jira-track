import { Interface } from "readline";
import { ask } from "./ask";

export async function askWithPlaceholder(
  rl: Interface,
  question: string,
  placeholder: string,
  validators: { [validator: string]: (input: string) => boolean } = {}
): Promise<string> {

  const askPromise = ask(rl, question, validators);
  
  rl.write(placeholder); 

  return askPromise; 
}