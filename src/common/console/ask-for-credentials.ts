import { MutableStdOut } from "./mutable-stdout";
import { createInterface, Interface } from "readline";
import { Credentials } from "../../core/jira-client";

const demandPassword = (rl: Interface, mutableStdout: MutableStdOut) => new Promise<string>((resolve) => {
  rl.question("password: ", (password) => {
    mutableStdout.unmute();
    resolve(password);
    process.stdout.write("\n");
  });
  mutableStdout.mute();
})

export async function askForCredentials(credentials: Partial<Credentials>): Promise<Credentials> {

  const mutableStdout = new MutableStdOut(),
    rl = createInterface(process.stdin, mutableStdout.stream, undefined, true);

  const username = credentials.username || await new Promise<string>((resolve) => rl.question("user: ", resolve)),
    password = await demandPassword(rl, mutableStdout);

  rl.close();
  process.stdout.write("\n");

  return { username, password };
}