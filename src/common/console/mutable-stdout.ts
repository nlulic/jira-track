import { Writable } from "stream";

export class MutableStdOut {

  public readonly stream = new Writable({
    write: (chunk, encoding, callback) => {
      if (!this.muted) {
        process.stdout.write(chunk, encoding);
      }
      callback();
    }
  });

  private muted = false;

  public mute() {
    this.muted = true;
  }

  public unmute() {
    this.muted = false;
  }
}