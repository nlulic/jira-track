import main from "./main";
import { storageClient } from "./core";

try {
  
  storageClient.initializeStorage();

} catch (error) {
  throw new Error("Storage could not be initialized.");
}

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";

main(process.argv);