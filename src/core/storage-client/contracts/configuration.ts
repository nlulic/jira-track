export interface Configuration {
  roundToNearestMinutes: number;
  username: string; 
  jiraUrl: string; 
}