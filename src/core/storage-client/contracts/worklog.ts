export interface Worklog {
  id: number; 
  endAt?: Date; 
  startAt: Date; 
  comment?: string; 
  issueKey: string;
  published?: boolean;
}