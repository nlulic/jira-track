import { Configuration } from "./configuration";
import { Worklog } from "./worklog";

export interface JiraTrackStorage {
  configuration: Configuration;
  worklogs: Worklog[]; 
}