import { getWorklogs, updatePublishedWorklog, updateWorklogs, updateWorklog, getWorklog } from "./worklogs";
import { updateConfiguration, getConfiguration } from "./configuration";
import { JiraTrackStorage } from "./contracts";
import store from "./store";
export * from "./contracts";

const initializeStorage = (): void => {

  store.defaults<JiraTrackStorage>({
    configuration: {
      roundToNearestMinutes: 5,
      username: "",
      jiraUrl: ""
    },
    worklogs: []
  }).write();
}

export const storageClient = {
  updateConfiguration,
  initializeStorage,
  getConfiguration,
  updatePublishedWorklog,
  updateWorklogs,
  updateWorklog,
  getWorklogs,
  getWorklog
}