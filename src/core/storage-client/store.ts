import { JiraTrackStorage } from "./contracts";
import FileSync from "lowdb/adapters/FileSync";
import { homedir } from "os";
import lowdb from "lowdb";
import path from "path";

const DB_NAME = "jira-track.db.json";

const adapter = new FileSync<JiraTrackStorage>(path.join(homedir(), "." + DB_NAME)),
  store = lowdb(adapter);

export default store;