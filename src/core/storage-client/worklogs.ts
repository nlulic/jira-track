import { Worklog } from "./contracts";
import store from "./store";

export const getWorklogs = (): Worklog[] => store.get("worklogs").valueOf();

export const getWorklog = (worklogId: number): Worklog | undefined => store.get("worklogs")
  .find(({ id }) => id === worklogId)
  .valueOf();

export const updateWorklogs = (worklogs: Worklog[]): void => store.set("worklogs", worklogs).write();

export const updateWorklog = ({ id, ...rest }: Worklog) => {

  const worklogs = store.get("worklogs")
    .map(worklog => id === worklog.id ? { ...worklog, ...rest } : worklog)
    .valueOf(); 

  return store.set("worklogs", worklogs).write(); 
};

export const updatePublishedWorklog = (worklogId: number) => store.get("worklogs").chain()
  .find({ id: worklogId })
  .assign({ published: true })
  .write();