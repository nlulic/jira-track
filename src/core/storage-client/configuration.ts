import { Configuration } from "./contracts";
import store from "./store";

export const getConfiguration = (): Configuration => store.get("configuration").valueOf();

export const updateConfiguration = (configuration: Configuration): void => store.set("configuration", configuration).write()