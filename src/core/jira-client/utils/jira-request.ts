import { JiraContext, JiraRequestOptions } from "../contracts";
import fetch from "node-fetch";
import * as Url from "url";

const DEFAULT_OPTIONS: JiraRequestOptions = { method: "GET" }

export async function jiraRequest<T>(path: string, context: JiraContext, options: JiraRequestOptions = DEFAULT_OPTIONS): Promise<T> {

  const { credentials, jiraUrl } = context;

  const url = new Url.URL(jiraUrl + path);

  try {
    const response = await fetch(url.toString(), {
      headers: {
        Authorization: `Basic ${Buffer.from(`${credentials.username}:${credentials.password}`).toString("base64")}`,
        'Content-Type': 'application/json'
      },
      method: options.method,
      body: options.body ? JSON.stringify(options.body) : undefined
    });

    if (!response.ok) 
      return Promise.reject(`request to ${url} failed: ${response.status} ${response.statusText}`);

    const jsonResponse = await response.json();

    return jsonResponse; 

  } catch (err) {
    console.error(`request to ${path} failed`)
    return Promise.reject(err);
  }
}