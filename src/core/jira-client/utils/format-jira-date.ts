import { format } from "date-fns"

// Formats the date to the required JIRA-API format. 
export function formatJiraDate(date: Date): string {
  return format(date, "yyyy-MM-dd'T'HH:mm:ss.SSSXXXX"); 
} 