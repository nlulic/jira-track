import { jiraRequest, formatJiraDate as formatDate } from "./utils";
import { JiraContext, JiraWorklog } from "./contracts";
import { Worklog } from "core/storage-client";

export * from "./contracts";
export * from "./iterators";

async function createWorklog({issueKey, comment, startAt, endAt }: Worklog, ctxt: JiraContext): Promise<JiraWorklog> {

  const workedSeconds = (new Date(endAt || "").getTime() - new Date(startAt).getTime()) / 1000;
  
  const payload = {
    started: formatDate(new Date()),
    timeSpentSeconds: workedSeconds,
    comment
  }

  const jiraWorklog = await jiraRequest<JiraWorklog>(`/rest/api/2/issue/${issueKey}/worklog`, ctxt, {method: "POST", body: payload});

  // return new Promise<JiraWorklog>(resolve => resolve()); 
  return jiraWorklog;
}

export const jiraClient = {
  createWorklog
}