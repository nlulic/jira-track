export interface JiraRequestOptions {
  method: "PUT" | "GET" | "POST"
  body?: Object;
}