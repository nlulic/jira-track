export interface JiraWorklog {
  self: string;
  author: JiraAuthor;
  updateAuthor: JiraAuthor;
  comment:
    | string
    | {
        type: string;
        version: number;
        content: Array<{
          type: string;
          content: Array<{
            type: string;
            text: string;
          }>;
        }>;
      };
  updated: string;
  started: string;
  timeSpent: string;
  timeSpentSeconds: number;
  id: number;
  issueId: number;
}

interface JiraAuthor {
  self: string;
  name: string;
  displayName: string;
  active: boolean;
  avatarUrls: { 
    '48x48': string 
    '32x32': string
    '24x24': string
    '16x16': string
  }
}