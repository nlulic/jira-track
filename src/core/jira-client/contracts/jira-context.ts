import { Credentials } from "./credentials";

export interface JiraContext {
  credentials: Credentials; 
  jiraUrl: string;
}