import { Worklog } from "../../storage-client";
import { JiraWorklog, JiraContext } from "../contracts";
import { jiraClient } from "..";

interface PublishResult {
  virtualWorklogId: number; // the id with which the worklog is persisted locally
  worklog: JiraWorklog;
}

const toResult = (worklog: JiraWorklog, virtualWorklogId: number): PublishResult => {
  return {
    virtualWorklogId,
    worklog
  }
}

export function* publishWorklogs(worklogs: Worklog[], ctxt: JiraContext): IterableIterator<Promise<PublishResult>> {

  for (const worklog of worklogs) {
    yield jiraClient.createWorklog(worklog, ctxt).then((jiraWorklog) => toResult(jiraWorklog, worklog.id))
  }
}