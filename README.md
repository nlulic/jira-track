# ⏳ jira-track

jira-track is a light CLI that tracks time spent working on tickets. Instead of keeping track of your work in an excel sheet, word document or a time-tracking GUI and later manually transferring those to JIRA, jira-track never forces you to leave your console and enables you to publish your tracked time directly through the CLI.

# How it Works

* Start tracking when you start working on a ticket 
* When you're done, either start tracking the next ticket or stop the tracking and write a comment for the work you did. 
* When your workday ends, simply publish your worklogs to JIRA

# Installation 

To install it globally run: 

`npm i -g jira-track`

# Getting Started

After the installation you can start time tracking right away by running: 

`jira-track start <ISSUE-KEY>`

When you're done with your task, simply stop tracking the issue: 

`jira-track stop`

or start tracking the next ticket. When you stop the tracking of a ticket, you will be asked to enter a comment for the work you've already done. 

## Help

To display a list of all commands run `jira-track --help`: 


```

Commands:
  jira-track configure                      Allows you to modify the internal
                                            configuration of jira-track
                                                               [Aliase: c, conf]
  jira-track start <jira-issue-key>         Starts the time tracking for a given
                                            issue key         [Aliase: s, track]
  jira-track stop                           Stops the time tracking for the
                                            latest active issue    [Aliase: end]
  jira-track edit <worklog-id>              allows you to modify the logged
                                            time. Get the worklog id by running
                                            `jira-track status --with-ids`
                                                                     [Aliase: e]
  jira-track replace <issue-key>            Replaces all occurrences of an issue
  <new-issue-key>                           key for a given date
  jira-track status [--date]                Displays a report of the worked time
                                            for a given date (default: Today)
                                                                [Aliase: report]
  jira-track publish [--date]               Publishes tracked worklogs to JIRA
                                                                     [Aliase: p]
  jira-track prune                          Deletes all published worklogs from
                                            the database         [Aliase: clear]

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```

# Commands

## start

The `start` command takes one required argument - the ticket key which you want to track. If you want to use the `publish` command later on, make sure that a ticket with the tracked key exists in JIRA. 

```
Starts the time tracking for a given issue key

Positionals:
  jira-issue-key                                                       [required]

Options:
  --version  Show version                                              [boolean]
  --help     Show help                                                 [boolean]
```

If you start tracking another ticket, it will prompt you to enter a comment for the work you have done on the last issue that is actively being tracked. 

## stop 

The `stop` command stops tracking the current issue. It will ask you to enter a comment to describe the work you've done on the ticket. 

## status (report)

The `status` command prints the tracked work for the day.

### Optional Arguments 

| Option | Description |
| ------ | ------ |
| --date | Let's you pass a date for which the status should be displayed |
| --raw | By default, the worked hours get rounded to the nearest 5 minutes. Using the option `--raw` will display the worktimes without rounding |  
| --with-ids | Displays the id of each worklog. This is used to edit the worklog | 

The output will look like this:

| Period | Worked | Issue | Comment | Published |
| ------ | ------ | ------ | ------ | ------ |
| 17:25 - 18:00| 35m | WFRED-3212 | Some comment | NO |
| 17:25 - 18:00 | 1h 30m | INFRA-263 | Another comment | NO |
| 17:25 - 18:00 | 2h 30m | XDEV-21 | Another comment | NO |
| Total: | **4h 35m** | | | |

Run `jira-track publish` to publish unpublished worklogs to jira

## edit 

Sometimes you'll need to edit worklogs, either because you have to change the issue key, or because you forgot to start tracking on time. To edit you first need the id of the worklog you want to modify. You get this by running `jira-track status --with-ids`. This command will display the id before each worklog. 

After than you can run `jira-track edit <worklog-id>` to edit the worklog. 

### Optional Arguments 

To make this process quicker, you can preset the values with the following arguments: 

| Option | Description |
| ------ | ------ |
| --start| the start time of the worklog (i.e. 15:30) |
| --end | the end time of the worklog | 
|  --comment | The worklogs comment |
| --key | The JIRA issue key | 

## replace 

Sometimes you don't know the issue key on which you are currently working. Maybe the issue wasn't created yet, but you still have to get the work done and track the time spent. 

This is where you can use the replace command. Start tracking your work with a made-up issue key like *UNKNOWN-1* and get on with your work. 

When you know the correct issue key for the work you did (let's say it's *FOO-25*), replace all occurrences of the made-up ticket by running `jira-track replace UNKOWN-1 FOO-25`. 

### Optional Arguments

| Option | Description |
| ------ | ------ |
| --date | The date from which the issue keys will be replaced. Default is today | 

## configure (c)

There is no configuration required to use `jira-track`, however there are some options which can be configured to make your life easier. 

### Optional Arguments

| Option | Description |
| ------ | ------ |
| --round | The minutes to which the tracked time is being rounded.<br> The default value is 5 but you can set it to any number between 1 and 30.<br> If you want to disable this, set this option to 1 |
| --jira-url | Specifies the base url with which the JIRA API can be accessed. This is used to publish the worklogs to JIRA | 
|  --username  | Your JIRA username. This is used for authentication | 

## publish 

Publishes the worklogs (only unpublished) to JIRA. This command requires the `jira-url` to be present in the configuration or passed as an argument. 

If no username is set in the configuration or passed as an argugment, the CLI will prompt for your JIRA username to authenticate.

### Optional Arguments

| Option | Description |
| ------ | ------ |
| --username | Your JIRA username |
| --jira-url | Specifies the base url with which the JIRA API can be accessed. This is used to publish the worklogs to JIRA |
| --date | The date from which the worklogs should be published. Default is today | 
| --raw | will publish the worklogs without rounding to the nearest minutes |

## prune

Deletes all worklogs that were already published from the local JSON database. 

### Optional Arguments

| Option | Description |
| ------ | ------ |
| --all | Removes all (even unpublished) worklogs |